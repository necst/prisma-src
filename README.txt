Team number: XIL98989
Project name: PrISMA
Date: 30/06/2016
Version of uploaded archive: 30/06/2016

University name: Politecnico di Milano
Supervisor name: Marco Santambrogio
Supervisor e-mail: marco.santambrogio@polimi.it
Participant(s): Gea Bianchi, Fabiola Casasopra
Email: gea.bianchi@mail.polimi.it
Email: fabiola.casasopra@mail.polimi.it

Board used: AVNET ZedBoard
Vivado Version: 2015.1
Brief description of project:
One of the most common protein identification method, called MS-MS Spectrometry, involves a computational intensive string matching problem. The procedure requires searching a list of small fragments (peptides) against the entire human protein database, in order to identify the composed protein. The complexity of the problem grows with the length of both the searched and the reference string and with the number of fragments. The task takes tens of seconds to complete and, if performed with GPP, this long execution time translates into a high energy requirement, which greatly impacts the scalability and maintenance cost of the system. This problem can affect the use of this application in large scale installations, such as medical or research centers, that work in the serum proteomic field. Moreover, within this field there are many researches oriented to cancer monitoring, such as gastric cancer[1], that are obtaining promising results and that will increase the amount of exams required, also in clinical application.In our project, we are going to speed up the string matching phase of biomarker proteins recognition and dropping the energy consumption, bringing the computational intensive task on a cluster of FPGAs. 
[1] W. Liu, Q. Yang, B. Liu, and Z. Zhu, “Serum proteomics for gastric cancer,” Clinica Chimica Acta, vol. 431, pp. 179–184, 2014.

Description of archive (explain directory structure, documents and source files):
Instructions to build and test project
Step 1:
...

Link to YouTube Video(s):

/*       ___
 *      /   /\
 *     /___/  \
 *     \___\  /_       \_\_\_         \_  \_\_\_   \_     \_  \_\_\_\_
 *    /   /\\/ /|     \_  \_         \_  \_       \_\_ \_\_  \_    \_
 *   /___/  \_/ |    \_\_\_  \_ \_  \_  \_\_\_   \_  \_ \_  \_\_\_\_
 *   \   \  / \ |   \_      \_\_   \_       \_  \_     \_  \_    \_
 *    \___\//\_\|  \_      \_     \_  \_\_\_   \_     \_  \_    \_
 *     /___/  \
 *     \   \  /
 *      \___\/
 *
 * Copyright 2016 Gea Bianchi, Fabiola Casasopra
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/time.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>

// Protein data structure
typedef struct {
    std::string UniqueIdentifier;
    std::string EntryName;
    std::string ProteinName;
    std::string Sequence;
} Protein;

// Vector to save the database
std::vector<Protein> database;

// Interval data structure
typedef struct{
    std::string functionName;
    uint64_t time;
    int peptideLength;
    int proteinLength;
} Interval;

// Vector to save the times
std::vector<Interval> intervals;

// Function to read the peptides from the file

int readPeptides(std::fstream *fPeptides, std::vector<std::string> *unknownProtein){
    std::string line;
    int validPeptides = 0;
    while (std::getline(*fPeptides, line)) {
        size_t pos = line.find_first_of("\t");
        std::string strPeptide = line.substr(pos+1);
        strPeptide = strPeptide + "y";
        if(validPeptides == 0 || strPeptide.compare(unknownProtein->at(validPeptides-1)) != 0){
            unknownProtein->push_back(strPeptide);
            validPeptides++;
        }
    }
    return validPeptides;
}

// Function to read the proteins from the file

void readProteins(std::fstream *fProteins, std::vector<Protein> *database){
    std::string line;
    while (std::getline(*fProteins, line)) {
        Protein prot;
        
        // Read unique identifier
        size_t startUniqueIdentifier = line.find_first_of("|");
        size_t endUniqueIdentifier = line.find_first_of("|", startUniqueIdentifier+1);
        prot.UniqueIdentifier = line.substr(startUniqueIdentifier+1, endUniqueIdentifier - (startUniqueIdentifier+1));
        
        // Read entry name
        size_t endEntryName = line.find_first_of(" ", endUniqueIdentifier+1);
        prot.EntryName = line.substr(endUniqueIdentifier+1, endEntryName - (endUniqueIdentifier+1));
        
        // Read protein name
        size_t endProteinName = line.find_first_of("=", endEntryName+1);
        prot.ProteinName = line.substr(endEntryName+1, endProteinName - (endEntryName+1) -3);
        
        // Read sequence
        std::streampos pos;
        while (std::getline(*fProteins, line)) {
            if (line.find_first_of(">") == 0) {
                break;
            }
            pos = (*fProteins).tellg();
            prot.Sequence+=line;
        }
        
        prot.Sequence += "x";
        database->push_back(prot);
        (*fProteins).seekg(pos, std::ios::beg);
    }
}

// Naive Algorithm

void naiveAlgorithm(std::string *peptide, std::string *protein, std::vector<int> *occurrence){
    
    int i, j, occ = 0;
    int n = (protein->length())-1;
    int m = (peptide->length())-1;
    
    for (i=0; i<n-m+1 ; i++) {
        j=0;
        while (j<m && peptide[j]==protein[i+j]) {
            j++;
        }
        if (j==m) {
            occurrence->push_back(i+1);
        }
    }
}

// Prefix Function

void prefixFunction(std::string *pattern, std::vector<int> *prefix){
    
    int i, h=0, left, right;
    int aux;
    int m = pattern->length();
    
    prefix->push_back(m);
    
    while (pattern->at(h) == pattern->at(1+h)) {
        h++;
    }
    
    prefix->push_back(h);
    left = 1;
    right = 1;
    
    for (i=2; i<m; i++) {
        aux = prefix->at(i-left);
        if (right<i) {
            h=0;
            
            while (pattern->at(h)==pattern->at(i+h)) {
                h++;
            }
            prefix->push_back(h);
            left = i;
            right = i+h-1;
        } else if (aux < right-i+1) {
            prefix->push_back(aux);
        } else {
            h=right-i+1;
            
            while (pattern->at(h)==pattern->at(i+h)) {
                h++;
            }
            
            prefix->push_back(h);
            left = i;
            right = i+h-1;
        }
    }
}

// KMP Algorithm

void KMPAlgorithm(std::string *peptide, std::string *protein, std::vector<int> *occurrence){
    
    std::vector<int> prefix;
    int h, i, j;
    int n = (protein->length())-1;
    int m = (peptide->length())-1;
    
    std::vector<int> d;
    
    prefixFunction(peptide, &prefix);
    
    for (j=0; j<m+1; j++) {
        d.push_back(j+1);
    }
    
    for (h=m; h>=0; h--) {
        d[h+prefix[h]] = h;
    }
    
    i=0;
    j=0;
    
    while (i<n-m+1) {
        while (peptide->at(j)==protein->at(i+j)) {
            j++;
        }
        
        if (j>m-1) {
            occurrence->push_back(i+1);
        }
        
        i = i+d[j];
        
        j = std::max(0,j-d[j]);
    }
}

// Function for the time

uint64_t getTime(){
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * 1000 * 1000 + tv.tv_usec;
}

// Function to print all execution interval

void executionInterval(std::vector<Interval> *intervals){
    
    int i;
    
    std::cout << "Function name" << "\t";
    std::cout << "Time" << "\t";
    std::cout << "Peptide length" << "\t";
    std::cout << "Protein length";
    std::cout << std::endl;
    
    for( i = 0; i < intervals->size(); i++ ){
        std::cout << (intervals->at(i)).functionName << "\t";
        std::cout << (intervals->at(i)).time << "\t";
        std::cout << (intervals->at(i)).peptideLength << "\t\t";
        std::cout << (intervals->at(i)).proteinLength;
        std::cout << std::endl;
    }
}

int main(int argc, char **argv){
    
    if(argc!=2){
        printf("USAGE: ./exe NUM_PROTEINS\n");
        exit(0);
    }
    
    // File to read the peptides of the unknown protein
    std::fstream fPeptides ("KI67_HUMAN.txt", std::fstream::in | std::fstream::out);
    
    // File to read the database
   	std::fstream fProteins ("uniprot_human_FASTA_canonical.txt", std::fstream::in | std::fstream::out);
    
    // Check if the files are opened correctly
    if (!fPeptides.is_open() || !fProteins.is_open()) {
        std::cout << "Error in opening text files" << std::endl;
        exit(0);
    }
    
    // Vector to save the peptides to be matched
    std::vector<std::string> unknownProtein;
    
    // Number of different peptides to match
    int validPeptides;
    
    // Number of protein of the database I want to check
    int numProteins = atoi(argv[1]);
    
    // Counter
    int i, j, k, p;
    
    // Time
    uint64_t start_database, start_protein, start_algorithm;
    uint64_t end_algorithm, end_protein, end_database;
    Interval interval;
    
    // Vector to save the matching position
    std::vector<int> occurrence;
    
    // Map to save the intermediate results
    // The key value is the unique identifier of the protein, the mapped value are the matching positions
    std::map<std::string, std::vector<int>> result;
    
    // Final result
    std::string idFound = "NULL";
    int maxDim = 0;
    
    /* Read from file... */
    
    // ...peptides...
    validPeptides = readPeptides(&fPeptides, &unknownProtein);
    
    // ...and proteins
   	readProteins(&fProteins, &database);
    
    // Check if the number of protein I want to match is less than the number of protein in the database
    numProteins = numProteins < database.size() ? numProteins : database.size();
    
    /* KMP Algorithm */
    
    start_database = getTime();
    
    // For each protein I want to check
    for (j=0; j<numProteins; j++) {
        start_protein = getTime();
        
        // For each peptide of the protein to be identified
        
        for (i=0; i<unknownProtein.size(); i++) {
            if (i==0 || unknownProtein[i].compare(unknownProtein[i-1])) {
                start_algorithm = getTime();
                KMPAlgorithm(&unknownProtein[i], &database[j].Sequence, &occurrence);
                end_algorithm = getTime();
                
                interval.functionName = "OneMatch";
                interval.time = end_algorithm - start_algorithm;
                interval.peptideLength = unknownProtein[i].size() - 1;
                interval.proteinLength = (database[j].Sequence).size() - 1;
                intervals.push_back(interval);
            }
            
        }
        
        end_protein = getTime();
        
        interval.functionName = "AllPeptides";
        interval.time = end_protein - start_protein;
        interval.peptideLength = 0; //this value has no meaning
        interval.proteinLength = (database[j].Sequence).size() - 1;
        intervals.push_back(interval);
        
        // Controlling if the string matching algorithm produced some results
        
        if (!occurrence.empty()){
            result[database[j].UniqueIdentifier] = occurrence;
            for(int e=0; e<occurrence.size(); e++)
                occurrence.erase(occurrence.begin() + e);
        }
        
    }
    
    end_database = getTime();
    
    interval.functionName = "Database";
    interval.time = end_database - start_database;
    interval.peptideLength = 0; //this value has no meaning
    interval.proteinLength = 0; //this value has no meaning
    intervals.push_back(interval);
    
    // Looking for the matching protein
    
    for (auto &h : result){
        if(h.second.size() > maxDim){
            idFound = h.first;
            maxDim = h.second.size();
        }
    }
    
    std::cout << "\n Protein identified with KMP algorithm: \n" << std::endl;
    std::cout << idFound << "\t" << maxDim;
    std::cout << std::endl;
    
    executionInterval(&intervals);
    
    return 0;
}
/*       ___
 *      /   /\
 *     /___/  \
 *     \___\  /_       \_\_\_         \_  \_\_\_   \_     \_  \_\_\_\_
 *    /   /\\/ /|     \_  \_         \_  \_       \_\_ \_\_  \_    \_
 *   /___/  \_/ |    \_\_\_  \_ \_  \_  \_\_\_   \_  \_ \_  \_\_\_\_
 *   \   \  / \ |   \_      \_\_   \_       \_  \_     \_  \_    \_
 *    \___\//\_\|  \_      \_     \_  \_\_\_   \_     \_  \_    \_
 *     /___/  \
 *     \   \  /
 *      \___\/
 *
 * Copyright 2016 Gea Bianchi, Fabiola Casasopra
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/time.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>

#include <netdb.h>
#include <netinet/in.h>

/* Protein data structure */
typedef struct {
    std::string UniqueIdentifier;
    std::string EntryName;
    std::string ProteinName;
    std::string Sequence;
} Protein;

/* Vector to save the database */
std::vector<Protein> database;

/* Function to read the peptides from the file */
int readPeptides(std::fstream *fPeptides, std::vector<std::string> *unknownProtein){
    std::string line;
    int validPeptides = 0;
    while (std::getline(*fPeptides, line)) {
        size_t pos = line.find_first_of("\t");
        std::string strPeptide = line.substr(pos+1);
        strPeptide = strPeptide + "y";
        if(validPeptides == 0 || strPeptide.compare(unknownProtein->at(validPeptides-1)) != 0){
            unknownProtein->push_back(strPeptide);
            validPeptides++;
        }
    }
    return validPeptides;
}

int main(int argc, char *argv[]) {
   int sockfd, portno, n;
   struct sockaddr_in serv_addr;
   struct hostent *server;
   
   char buffer[256];

   /* File to read the peptides of the unknown protein */
   std::fstream fPeptides ("KI67_HUMAN.txt", std::fstream::in | std::fstream::out);
    
   /* Check if the file is opened correctly */
   if (!fPeptides.is_open()) {
       std::cout << "Error in opening text files" << std::endl;
       exit(0);
   }
    
   /* Vector to save the peptides to be matched */
   std::vector<std::string> unknownProtein;
    
   /* Number of different peptides to match */
   int validPeptides;

    
   /* Counter */
   int i, j, k, p;
    
   // Time
   uint64_t start_database, start_protein, start_algorithm;
   uint64_t end_algorithm, end_protein, end_database;
    
   // Vector to save the matching position
   std::vector<int> occurrence;
    
   // Map to save the intermediate results
   // The key value is the unique identifier of the protein, the mapped value are the matching positions
   std::map<std::string, std::vector<int>> result;
    
   // Final result
   std::string idFound = "NULL";
   int maxDim = 0;
   
   if (argc < 3) {
      fprintf(stderr,"usage %s hostname port \n", argv[0]);
      exit(0);
   }
	
   portno = atoi(argv[2]);
   
   /* Create a socket point */
   sockfd = socket(AF_INET, SOCK_STREAM, 0);
   
   if (sockfd < 0) {
      perror("ERROR opening socket");
      exit(1);
   }
	
   server = gethostbyname(argv[1]);
   
   if (server == NULL) {
      fprintf(stderr,"ERROR, no such host\n");
      exit(0);
   }
   
   bzero((char *) &serv_addr, sizeof(serv_addr));
   serv_addr.sin_family = AF_INET;
   bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
   serv_addr.sin_port = htons(portno);
   
   /* Now connect to the server */
   if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
      perror("ERROR connecting");
      exit(1);
   }
    
   /* Read from file... */
    
   // ...peptides...
   validPeptides = readPeptides(&fPeptides, &unknownProtein);
   
   for (i=0; i<unknownProtein.size(); i++) {
      if (i==0 || unknownProtein[i].compare(unknownProtein[i-1])) {
         /* Now ask for a message from the user, this message
          * will be read by server
          */

         bzero(buffer,256);
	 strcpy(buffer, unknownProtein[i].c_str());         
        
         /* Send message to the server */
         n = write(sockfd, buffer, strlen(buffer));
   
         if (n < 0) {
            perror("ERROR writing to socket");
            exit(1);
         }
           
         /* Now read server response */
   	 bzero(buffer,256);
   	 n = read(sockfd, buffer, 255);

 	 if (n < 0) {
      	    perror("ERROR reading from socket");
            exit(1);
         }
      }      
   }

   bzero(buffer,256);
   strcpy(buffer, "End");         
        
   /* Send message to the server */
   n = write(sockfd, buffer, 3);

   /* Now read server response */
   bzero(buffer,256);
   n = read(sockfd, buffer, 255);

   if (n < 0) {
      perror("ERROR reading from socket");
      exit(1);
   }

   std::cout << "message: " << buffer << std::endl;

   /* Now read server response */
   bzero(buffer,256);
   n = read(sockfd, buffer, 255);

   if (n < 0) {
      perror("ERROR reading from socket");
      exit(1);
   }

   std::cout << "message: " << buffer << std::endl;

   return 0;
}

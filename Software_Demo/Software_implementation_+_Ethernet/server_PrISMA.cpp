/*       ___
 *      /   /\
 *     /___/  \
 *     \___\  /_       \_\_\_         \_  \_\_\_   \_     \_  \_\_\_\_
 *    /   /\\/ /|     \_  \_         \_  \_       \_\_ \_\_  \_    \_
 *   /___/  \_/ |    \_\_\_  \_ \_  \_  \_\_\_   \_  \_ \_  \_\_\_\_
 *   \   \  / \ |   \_      \_\_   \_       \_  \_     \_  \_    \_
 *    \___\//\_\|  \_      \_     \_  \_\_\_   \_     \_  \_    \_
 *     /___/  \
 *     \   \  /
 *      \___\/
 *
 * Copyright 2016 Gea Bianchi, Fabiola Casasopra
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/time.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>

#include <netdb.h>
#include <netinet/in.h>

/* Protein data structure */
typedef struct {
    std::string UniqueIdentifier;
    std::string EntryName;
    std::string ProteinName;
    std::string Sequence;
} Protein;

/* Vector to save the database */
std::vector<Protein> database;

/* Function to read the proteins from the file */
void readProteins(std::fstream *fProteins, std::vector<Protein> *database){
   std::string line;
   while (std::getline(*fProteins, line)) {
   Protein prot;
        
   /* Read unique identifier */
   size_t startUniqueIdentifier = line.find_first_of("|");
   size_t endUniqueIdentifier = line.find_first_of("|", startUniqueIdentifier+1);
   prot.UniqueIdentifier = line.substr(startUniqueIdentifier+1, endUniqueIdentifier - (startUniqueIdentifier+1));
        
   /* Read entry name */
   size_t endEntryName = line.find_first_of(" ", endUniqueIdentifier+1);
   prot.EntryName = line.substr(endUniqueIdentifier+1, endEntryName - (endUniqueIdentifier+1));
        
   /* Read protein name */
   size_t endProteinName = line.find_first_of("=", endEntryName+1);
   prot.ProteinName = line.substr(endEntryName+1, endProteinName - (endEntryName+1) -3);
        
   /* Read sequence */
   std::streampos pos;
   while (std::getline(*fProteins, line)) {
      if (line.find_first_of(">") == 0) {
         break;
      }
      pos = (*fProteins).tellg();
      prot.Sequence+=line;
   }
        
   prot.Sequence += "x";
   database->push_back(prot);
   (*fProteins).seekg(pos, std::ios::beg);
   }
}

/* Prefix Function */
void prefixFunction(std::string *pattern, std::vector<int> *prefix){
   int i, h=0, left, right;
   int aux;
   int m = pattern->length();
    
   prefix->push_back(m);
    
   while (pattern->at(h) == pattern->at(1+h)) {
      h++;
   }
    
   prefix->push_back(h);
   left = 1;
   right = 1;
    
   for (i=2; i<m; i++) {
      aux = prefix->at(i-left);
      if (right<i) {
         h=0;
            
         while (pattern->at(h)==pattern->at(i+h)) {
            h++;
         }
         prefix->push_back(h);
            left = i;
            right = i+h-1;
         } else if (aux < right-i+1) {
            prefix->push_back(aux);
         } else {
            h=right-i+1;
            
         while (pattern->at(h)==pattern->at(i+h)) {
            h++;
         }
            
         prefix->push_back(h);
         left = i;
         right = i+h-1;
      }
   }
}

/* KMP Algorithm */
void KMPAlgorithm(std::string *peptide, std::string *protein, std::vector<int> *occurrence){ 
   std::vector<int> prefix;
   int h, i, j;
   int n = (protein->length())-1;
   int m = (peptide->length())-1;    
   std::vector<int> d;
    
   prefixFunction(peptide, &prefix);
    
   for (j=0; j<m+1; j++) {
      d.push_back(j+1);
   }
    
   for (h=m; h>=0; h--) {
      d[h+prefix[h]] = h;
   }
    
   i=0;
   j=0;
    
   while (i<n-m+1) {
      while (peptide->at(j)==protein->at(i+j)) {
         j++;
      }
        
      if (j>m-1) {
         occurrence->push_back(i+1);
      }
        
      i = i+d[j];
        
      j = std::max(0,j-d[j]);
   }
}

int main( int argc, char *argv[] ) {
   int sockfd, newsockfd, portno, clilen;
   char buffer[256];
   std::string buff;
   struct sockaddr_in serv_addr, cli_addr;
   int  n;
   int i, j;
   int loop = 1;
   
   /* Vector to save the peptides to be matched */
   std::vector<std::string> unknownProtein;

   /* Vector to save the matching position */
   std::vector<int> occurrence;
    
   /* Map to save the intermediate results */
   /* The key value is the unique identifier of the protein, the mapped value are the matching positions */
   std::map<std::string, std::vector<int>> result;
    
   /* Final result */
   std::string idFound = "NULL";
   int maxDim = 0;

   /* File to read the database */
   std::fstream fProteins ("uniprot_human_FASTA_canonical.txt", std::fstream::in | std::fstream::out);
    
   /* Check if the file are opened correctly */
   if (!fProteins.is_open()) {
      std::cout << "Error in opening text files" << std::endl;
      exit(0);
   }

   /* Reading proteins... */
   readProteins(&fProteins, &database);

   /* First call to socket() function */
   sockfd = socket(AF_INET, SOCK_STREAM, 0);
   
   if (sockfd < 0) {
      perror("ERROR opening socket");
      exit(1);
   }
   
   /* Initialize socket structure */
   bzero((char *) &serv_addr, sizeof(serv_addr));
   portno = 5002;
   
   serv_addr.sin_family = AF_INET;
   serv_addr.sin_addr.s_addr = INADDR_ANY;
   serv_addr.sin_port = htons(portno);
   
   /* Now bind the host address using bind() call.*/
   if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
      perror("ERROR on binding");
      exit(1);
   }
      
   /* Now start listening for the clients, here process will
    * go in sleep mode and will wait for the incoming connection
    */
   
   listen(sockfd,5);
   clilen = sizeof(cli_addr);
   
   /* Accept actual connection from the client */
   newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, (socklen_t*)&clilen);
	
   if (newsockfd < 0) {
      perror("ERROR on accept");
      exit(1);
   }
   
   while(loop){
      /* If connection is established then start communicating */
      bzero(buffer,256);
      n = read( newsockfd,buffer,255 );

      if (n < 0) {
         perror("ERROR reading from socket");
         exit(1);
      }

      buff.assign(buffer,n);

      if (n != 0)
         std::cout << "Here is the message: " << buff << std::endl;

      if (buff.compare(0,3,"End") == 0){
         loop = 0;
	 /* Write a response to the client */
         n = write(newsockfd,"All peptide sent",16);
   	
         if (n < 0) {
            perror("ERROR writing to socket");
            exit(1);
         }
      } else {

         unknownProtein.push_back(buff);
   
         /* Write a response to the client */
         n = write(newsockfd,"Peptide sent",12);
   
         if (n < 0) {
            perror("ERROR writing to socket");
            exit(1);
         }
      }
   }  

   for (j=0; j<database.size(); j++) {
      for (i=0; i<unknownProtein.size(); i++) {
         KMPAlgorithm(&unknownProtein[i], &database[j].Sequence, &occurrence);
      }

      if (!occurrence.empty()){
         result[database[j].UniqueIdentifier] = occurrence;
         for(int e=0; e<occurrence.size(); e++)
            occurrence.erase(occurrence.begin() + e);
      }
   }   
   
   for (auto &h : result){
       if(h.second.size() > maxDim){
           idFound = h.first;
           maxDim = h.second.size();
       }
   }
    
   std::cout << "\n Protein identified with KMP algorithm: \n" << std::endl;
   std::cout << idFound << "\t" << maxDim;
   std::cout << std::endl;

   buff.assign("Protein identified with KMP algorithm: ");
   buff.append(idFound);
   buff.append(" in which ");
   buff.append(std::to_string(maxDim));
   buff.append(" peptides were found.");

   /* Write a response to the client */
   bzero(buffer,256);
   strcpy(buffer, buff.c_str());

   n = write(newsockfd,buffer,256);
   
   if (n < 0) {
      perror("ERROR writing to socket");
      exit(1);
   }

   return 0;
}

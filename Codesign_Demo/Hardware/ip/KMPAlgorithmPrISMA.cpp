/*       ___
 *      /   /\
 *     /___/  \
 *     \___\  /_       \_\_\_         \_  \_\_\_   \_     \_  \_\_\_\_
 *    /   /\\/ /|     \_  \_         \_  \_       \_\_ \_\_  \_    \_
 *   /___/  \_/ |    \_\_\_  \_ \_  \_  \_\_\_   \_  \_ \_  \_\_\_\_
 *   \   \  / \ |   \_      \_\_   \_       \_  \_     \_  \_    \_
 *    \___\//\_\|  \_      \_     \_  \_\_\_   \_     \_  \_    \_
 *     /___/  \
 *     \   \  /
 *      \___\/
 *
 * Copyright 2016 Gea Bianchi, Fabiola Casasopra
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ap_int.h>
#include <hls_stream.h>

# define M 160 
# define N 125000

#define D_TYPE int

#define LOAD_DATABASE 0
#define PROTEIN_MATCH 1

template<int D> struct ap_axis{
    ap_int<D> data;
    ap_uint<1> last;
  };

template struct my_ap_axis_struct{
    D_TYPE data;
    bool last;
  };

typedef ap_axis<32> AXI_VAL;

typedef struct my_ap_axis_struct MY_TYPE_VAL;

typedef hls::stream<AXI_VAL> AXI_STREAM_VAL;

MY_TYPE_VAL operator + (const MY_TYPE_VAL& lhs, const MY_TYPE_VAL &rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs.data + rhs.data;
	return temp;
}

MY_TYPE_VAL operator - (const MY_TYPE_VAL& lhs, const MY_TYPE_VAL &rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs.data - rhs.data;
	return temp;
}

MY_TYPE_VAL operator * (const D_TYPE& lhs, const MY_TYPE_VAL &rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs * rhs.data;
	return temp;
}

MY_TYPE_VAL operator * (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs.data * rhs;
	return temp;
}

MY_TYPE_VAL operator * (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs.data * rhs.data;
	return temp;
}

MY_TYPE_VAL operator / (const D_TYPE& lhs, const MY_TYPE_VAL &rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs / rhs.data;
	return temp;
}

MY_TYPE_VAL operator / (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs.data / rhs;
	return temp;
}

MY_TYPE_VAL operator / (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs.data / rhs.data;
	return temp;
}

bool operator == (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	return lhs.data == rhs.data;
}

bool operator == (const D_TYPE &lhs, const MY_TYPE_VAL& rhs) {
	return lhs == rhs.data;
}

bool operator == (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data == rhs;
}

bool operator < (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	return lhs.data < rhs.data;
}

bool operator <= (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	return lhs.data <= rhs.data;
}

bool operator > (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	return lhs.data > rhs.data;
}

bool operator >= (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	return lhs.data >= rhs.data;
}

bool operator > (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data > rhs;
}

bool operator >= (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data >= rhs;
}

bool operator > (const D_TYPE& lhs, const MY_TYPE_VAL &rhs ) {
	return lhs > rhs.data;
}

bool operator >= (const D_TYPE& lhs, const MY_TYPE_VAL &rhs ) {
	return lhs >= rhs.data;
}

bool operator < (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data < rhs;
}

bool operator <= (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data <= rhs;
}

bool operator < (const D_TYPE& lhs, const MY_TYPE_VAL &rhs ) {
	return lhs < rhs.data;
}

bool operator <= (const D_TYPE& lhs, const MY_TYPE_VAL &rhs ) {
	return lhs <= rhs.data;
}

D_TYPE operator - (const D_TYPE &lhs, const MY_TYPE_VAL& rhs) {
	return lhs - rhs.data;
}

D_TYPE operator - (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data - rhs;
}

D_TYPE operator + (const D_TYPE &lhs, const MY_TYPE_VAL& rhs) {
	return lhs + rhs.data;
}

D_TYPE operator + (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data + rhs;
}

D_TYPE operator % (const MY_TYPE_VAL &lhs, const D_TYPE &rhs) {
	return lhs.data % rhs;
}

void operator >> (AXI_STREAM_VAL& edge, MY_TYPE_VAL &data) {
	AXI_VAL temp;
	union
	{
		int ival;
		D_TYPE oval;
	} converter;

	edge >> temp;
	converter.ival = temp.data;
	data.data = converter.oval;
}

void operator << (AXI_STREAM_VAL& edge, MY_TYPE_VAL data) {
	AXI_VAL temp;
	union
	{
		int oval;
		D_TYPE ival;
	} converter;

	converter.ival = data.data;
	temp.data = converter.oval;
	temp.last = data.last;
	edge << temp;
}

int Max(int a, int b){
	if(a > b)
		return a;
	else
		return b;
}

void FunzionePrefisso(char *S, int n, int pref[]){

    int i;
    int h = 0, l, r;

    pref[0] = n;

    while(S[h] == S[1+h])
        h++;

    pref[1] = h;
    l = 1;
    r = 1;

    for(i=2; i<n ; i++){
        if(r<i){
            h = 0;

            while(S[h] == S[i+h])
                h++;

            pref[i] = h;
            l = i;
            r = i+h-1;
        }

        else if(pref[i-l] < r-i+1)
            pref[i] = pref[i-l];

        else {
            h = r-i+1;

            while(S[h] == S[i+h])
                h++;

            pref[i] = h;
            l = i;
            r = i+h-1;
        }
    }
}

void KMPAlgorithmPrISMA(AXI_STREAM_VAL &DMA_In, AXI_STREAM_VAL &DMA_Out){
	
	MY_TYPE_VAL cmd; // To decide if loading DB or startintg the protein match
	MY_TYPE_VAL intData; // To read data as integer
	MY_TYPE_VAL n, m; // Database && each peptide length
	MY_TYPE_VAL numOfPept; // Number of peptides to check

	char Tchar[N]; // Array to save the database 
	char Pchar[M]; // Array to save peptides

	int pref[M]; // Array for KMP preelaboration
	int d[M]; // Array for KMP preelaboration

	MY_TYPE_VAL res; // Position of matching
	MY_TYPE_VAL proteinFound; // Number of protein matched

	int i, j, h, k; // Counters

	int numOfInt; // How many int are made by the char
	int numOfRemainingChars; // How many char do not form a entierly valid int
	int numCharIt; // Number of valid char in the int
	int mask = 0xFF; // Mask to read each char

	int proteinNumber = 0; // Number of the protein I'm checking
	int count = -1; // Position of the begin of the protein I'm checking

	while(1){
	DMA_In >> cmd;

		// Setup phase (database)
		if(cmd == LOAD_DATABASE){

			DMA_In >> n; // Sending database length
			numOfInt = n.data/4;
			numOfRemainingChars = n%4;

			for(i=0; i<=numOfInt; i++){
				if(i == numOfInt && numOfRemainingChars == 0)
					break;

				DMA_In >> intData;

				if(i == numOfInt)
					numCharIt = numOfRemainingChars;
				else
					numCharIt = 4;

				for(j = 0; j < numCharIt; ++j)
					Tchar[i*4+j] = (intData.data >> (8*j)) & mask; 
			}
		}

		// Computational phase (matching)
		if(cmd == PROTEIN_MATCH){

			DMA_In >> numOfPept; // How many pept I have to check
			for(k=0; k<numOfPept; k++){

				// Sending peptide length
				DMA_In >> m;
				numOfInt = m.data/4;
				numOfRemainingChars = m%4;
		
				// Sending peptdide sequence
				for (i=0; i<=numOfInt; i++){
					if(i == numOfInt && numOfRemainingChars == 0)
						break;

					DMA_In >> intData;
	
					if (i == numOfInt)
						numCharIt = numOfRemainingChars;
					else
						numCharIt = 4;

					for (j=0; j<numCharIt; ++j)
						Pchar[i*4+j] = (intData.data >> (8*j)) & mask; 
				}

				// Pre-elaborazion
				FunzionePrefisso(Pchar, m.data, &pref[0]);

				for(j=0; j<m; j++)
					d[j] = j+1;

				for(h=m-1; h>=0; h--)
					d[h+pref[h]] = h;

				// Initialize variable
				i = 0;
				j = 0;
				proteinNumber = 0;
				count = -1;

				while(i<n.data-m.data+1){

					// Check if it starts a new protein
					if(Tchar[i] == 'z'){
						proteinNumber++;
						count = i;
					}

					while(Pchar[j] == Tchar[i + j])
        	   				j++;

        				if(j>m.data-2){			
	
						// Return the number of protein
						proteinFound.data = proteinNumber;
						proteinFound.last = 1;
						DMA_Out << proteinFound;
				
						// Return the position in the protein
						res.data = (i - count);
			  			res.last = 1;
			  			DMA_Out << res;

        				}

        				i = i+d[j];
        				j = Max(0, j-d[j]);

    				}

				// When -1 is returned, the peptide match is finished
				res.data = -1;
				res.last = 1;
				DMA_Out << res;
			}
		}
	}
}

/*       ___
 *      /   /\
 *     /___/  \
 *     \___\  /_       \_\_\_         \_  \_\_\_   \_     \_  \_\_\_\_
 *    /   /\\/ /|     \_  \_         \_  \_       \_\_ \_\_  \_    \_
 *   /___/  \_/ |    \_\_\_  \_ \_  \_  \_\_\_   \_  \_ \_  \_\_\_\_
 *   \   \  / \ |   \_      \_\_   \_       \_  \_     \_  \_    \_
 *    \___\//\_\|  \_      \_     \_  \_\_\_   \_     \_  \_    \_
 *     /___/  \
 *     \   \  /
 *      \___\/
 *
 * Copyright 2016 Gea Bianchi, Fabiola Casasopra
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/time.h>
#include <stdio.h>

#include <iostream>
#include <fstream> 
#include <vector>
#include <map>

/* Linux HW support */
#include "supportLib.h"
#include <fcntl.h>

/* Max length of a protein */
#define N 35000
/* Max length of a peptide */
#define M 160
/* Max length of the database that can be sent -> numero abbastanza a caso per avere 4 pezzi di db */
#define DB 125000

/* Commands */
#define LOAD_DATABASE 0
#define PROTEIN_MATCH 1

typedef struct {
   std::string UniqueIdentifier;
   std::string EntryName;
   std::string ProteinName;
   std::string Sequence;
} Protein;

typedef std::map<int, std::vector<int>> InnerMap;
typedef std::map<int, InnerMap> OuterMap; 
typedef OuterMap::iterator OuterIterMap;
typedef InnerMap::iterator InnerIterMap;

/* Read the peptides */
int readPeptides(std::fstream *fPeptides, std::vector<std::string> *unknownProtein){
   std::string line;
   int validPeptides = 0;
   while (std::getline(*fPeptides, line)) {
      size_t pos = line.find_first_of("\t");
      std::string strPeptide = line.substr(pos+1);
      strPeptide = strPeptide + "y";
      if(validPeptides == 0 || strPeptide.compare(unknownProtein->at(validPeptides-1)) != 0){
         unknownProtein->push_back(strPeptide);
	 validPeptides++;
      }
   }
   return validPeptides;
}

/* Read the proteins */
void readProteins(std::fstream *fProteins, std::map<int, std::vector<Protein>> *database){
   std::string line;
   int conf = 0;
   int dbSize = 0;
   while (std::getline(*fProteins, line)) {
      Protein prot;
        
      /* Read unique identifier */
      size_t startUniqueIdentifier = line.find_first_of("|");
      size_t endUniqueIdentifier = line.find_first_of("|", startUniqueIdentifier+1);
      prot.UniqueIdentifier = line.substr(startUniqueIdentifier+1, endUniqueIdentifier - (startUniqueIdentifier+1));       

      /* Read entry name */
      size_t endEntryName = line.find_first_of(" ", endUniqueIdentifier+1);
      prot.EntryName = line.substr(endUniqueIdentifier+1, endEntryName - (endUniqueIdentifier+1));       	

      /* Read protein name */
      size_t endProteinName = line.find_first_of("=", endEntryName+1);
      prot.ProteinName = line.substr(endEntryName+1, endProteinName - (endEntryName+1) -3);
        	
      /* Read sequence */
      std::streampos pos;
      while (std::getline(*fProteins, line)) {
         if (line.find_first_of(">") == 0) {
            break;
         }
         pos = (*fProteins).tellg();
         prot.Sequence+=line;
      }
	
      prot.Sequence += "z";
      dbSize += (prot.Sequence).length();
      if(dbSize < DB){
         (*database)[conf].push_back(prot);
      } else {
         dbSize = 0;
	 conf++;
      }
      (*fProteins).seekg(pos, std::ios::beg);
   }
}

/* How many int I need to save the char */
unsigned int roundToInt(unsigned int l){
   return l/4 + (l%4!=0);
}

/* Sending core configuration */
void sendCommand(int fd, int cmd){
   writeDMA(fd, (unsigned char *) &cmd, sizeof(int));
}

/* Sending proteins */
void sendProteins(int fd, unsigned char *sequence){
   int i;
   unsigned int databaseLength = 0,databaseLengthRounded = 0;

   databaseLength = strlen((char*) sequence);
   databaseLengthRounded = roundToInt(databaseLength); 

   /* Send protein length */
   writeDMA(fd, (unsigned char *) &databaseLength, sizeof(int));

   /* Padding */
   if((databaseLength%4)!=0)
      for(i=0; i<(4-(databaseLength%4)); i++)
         strcat((char *)sequence, "x");

   /* Send protein sequence */
   writeDMA(fd, (unsigned char *) sequence, databaseLengthRounded * sizeof(int));
}

/* Sending the number of valid peptides */
void sendNumPept(int fd, int num){
   writeDMA(fd, (unsigned char *) &num, sizeof(int));
}

/* Sending peptides */
void sendPeptide(int fd, unsigned char *peptide){
   int i;
   unsigned int peptSize = strlen((char *)peptide);
   unsigned int peptSizeRounded = roundToInt(peptSize);
   unsigned char paddedPeptide[M];

   strcpy((char*)paddedPeptide, (char*)peptide);

   /* Padding */
   if((peptSize%4)!=0)
      for(i=0; i<(4-(peptSize%4)); i++)
         strcat((char *)paddedPeptide, "x");

      /* Send peptide size */
      writeDMA(fd, (unsigned char *) &peptSize, sizeof(int));
	
      /* Send peptide sequence */
      std::cout << "Sending peptide " << peptide << "of size" << peptSize << " to dma " << fd << std::endl;
      writeDMA(fd, (unsigned char *) paddedPeptide, peptSizeRounded * sizeof(int));
}

/* Checking results */
void checkResults(int fd, int conf, OuterMap *intermediateResult){
   int numProtein, position;

   do{
      readDMA(fd, (unsigned char *) &numProtein, sizeof(int));
      //if(numProtein == -1)
         //std::cout << "No match found! Num = " << numProtein << std::endl;

      if(/*r!=2147483647 || */numProtein!= -1){
         readDMA(fd, (unsigned char *) &position, sizeof(int));

	 std::cout << "Save the position of the match " << position << " in the protein ";
	 std::cout << numProtein << " with conf " << conf << std::endl;
	 ((*intermediateResult)[conf][numProtein]).push_back(position);
         // std::cout << "intermediateResult size is: " << (*intermediateResult).size() << std::endl;
      }
   }while(/*r!=2147483647 || */numProtein!= -1);
}


int main(){
	
   std::vector<std::string> unknownProtein; // Vector to save the peptides to be matched
   std::map<int, std::vector<Protein>> database; // Map to save the database
   std::fstream fPeptides ("KI67_HUMAN.txt", std::fstream::in | std::fstream::out); // File to read the peptides of the unknown protein
   std::fstream fProteins ("uniprot_human_FASTA_canonical.txt", std::fstream::in | std::fstream::out); // File to read the database
   if (!fPeptides.is_open() || !fProteins.is_open()) {
      std::cout << "Error in opening text files" << std::endl;
      exit(0);
   }
   int validPeptides; // Number of different peptides to match
   unsigned char prot[N], pept[M]; // Array to send the char
   int i, j, k, p; // Counter
   OuterMap intermediateResult;
   int dma0 = open("/dev/axi-dma1", O_RDWR); // Dmas to send the data and read them
   int dma1 = open("/dev/axi-dma2", O_RDWR);
   int dma2 = open("/dev/axi-dma3", O_RDWR);
   int dma3 = open("/dev/axi-dma4", O_RDWR);
   int dma[4] = {dma0, dma1, dma2, dma3};
   if(dma0<0 || dma1<0 || dma2<0 || dma3<0){
      std::cout << "Error in opening file" << std::endl;
      exit(0);
   }
	
   /* read from file */
      	
   /* peptides */
   validPeptides = readPeptides(&fPeptides, &unknownProtein);
    	
   /* proteins*/
   readProteins(&fProteins, &database); 

   int dbSize = database.size()/4;
   int remainingSize = database.size()%4;

   int numParts = 0;

   /* communication with hw */
   for(j=0; j<=dbSize; j++){
      if(j == dbSize && remainingSize == 0)
         break;

      if(j == dbSize)
         numParts = remainingSize;
      else
	 numParts = 4;

   /* sending db parts to the 4 dmas */

      for(i=0; i<numParts; i++){

         // std::cout << "Set number :" << j*4+i << std::endl;
	 // std::cout << "Dma number :" << i << std::endl;
      	 // std::cout << "Sending load command" << std::endl;

	 /* Send load command */
	 sendCommand(dma[i], LOAD_DATABASE);

	 /* Send the part of database */
	 unsigned char Sdatabase[DB] = ""; // Array to send each part of the database
	 for(k=0; k<database[j*4+i].size(); k++){
	    strcpy((char*)prot, (database[j*4+i][k].Sequence).c_str());
	    strcat((char *)Sdatabase, (char*)prot);
	 }

	 sendProteins(dma[i], Sdatabase);

	 // std::cout << "Sending match command" << std::endl;

	 /* Send match command */
	 sendCommand(dma[i], PROTEIN_MATCH);

         /* Send number of peptide I have to match */
	 sendNumPept(dma[i], validPeptides);

      }

      /* sending peptides to the 4 dmas and waiting for the results*/
      for(p=0; p<validPeptides; p++){
         strcpy((char *)pept, unknownProtein[p].c_str());
	 for(i=0; i<numParts; i++){
	    sendPeptide(dma[i], pept);
	 }
	 for(i=0; i<numParts; i++){
	    // std::cout << "reading results from" << i << "with conf" << j*4+i << std::endl;
	    checkResults(dma[i], j*4+i, &intermediateResult);
	 }
      }
   }
    
   // std::cout << "intermediateResult size is: " << intermediateResult.size() << std::endl;
   // for( OuterIterMap o = intermediateResult.begin(); o != intermediateResult.end(); ++o )
      // for( InnerIterMap i = o->second.begin(); i != o->second.end(); ++i )
         // std::cout << "Configuration: " << o->first << " -> protein: " << i->first << std::endl;
         // for( int k=0; k<(i->second).size(); ++k)
            // std::cout << " -> " << i->second[k] << std::endl;

   std::map<std::string, int> finalResult;
   
   for ( int cf=0; cf<intermediateResult.size(); ++cf ){
      for( InnerIterMap i = intermediateResult[cf].begin(); i != intermediateResult[cf].end(); ++i ){
         int prot = i->first;
         std::string UId = database[cf][prot].UniqueIdentifier;
         int numOcc = (i->second).size();
         finalResult[UId] = numOcc;
      }   
   }

   //for (std::map<std::string, int>::iterator h = finalResult.begin(); h != finalResult.end(); ++h )
      //std::cout << "Protein " << h->first << " -> " << h->second << std::endl;

   int maxDim = 0;
   std::string idFound;

   for (auto &h : finalResult){
        if(h.second > maxDim){
            idFound = h.first;
            maxDim = h.second;
        }
    }
    
    std::cout << "\n Protein identified with KMP algorithm: \n" << std::endl;
    std::cout << idFound << "\t" << maxDim;
    std::cout << std::endl;

   return 0;
}


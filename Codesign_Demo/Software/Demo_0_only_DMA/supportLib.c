/*       ___
 *      /   /\
 *     /___/  \
 *     \___\  /_       \_\_\_         \_  \_\_\_   \_     \_  \_\_\_\_
 *    /   /\\/ /|     \_  \_         \_  \_       \_\_ \_\_  \_    \_
 *   /___/  \_/ |    \_\_\_  \_ \_  \_  \_\_\_   \_  \_ \_  \_\_\_\_
 *   \   \  / \ |   \_      \_\_   \_       \_  \_     \_  \_    \_
 *    \___\//\_\|  \_      \_     \_  \_\_\_   \_     \_  \_    \_
 *     /___/  \
 *     \   \  /
 *      \___\/
 *
 * Copyright 2016 Gianluca Durelli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "supportLib.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>

unsigned long getTime(){
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec * 1000000 + tv.tv_usec;
}

unsigned long reconfigure(char *bitstream){
	char buf[1024];

	unsigned long start, end;

	start = getTime();
	system("echo 1 > /sys/devices/amba.0/f8007000.ps7-dev-cfg/is_partial_bitstream");

	sprintf(buf, "cat %s > /dev/xdevcfg", bitstream);
	printf("Issuing reconfiguration of %s\n", bitstream);
	system(buf);
	end = getTime();

	return end-start;
}

unsigned long readDMA(int fd, unsigned char *buffer, unsigned long length){
	unsigned long byteMoved = 0;
	unsigned long byteToTransfer = 0;
	unsigned long start, end;

	printf("Start reading...\n"); fflush(NULL);

	start = getTime();
	while(byteMoved != length){
		byteToTransfer = length - byteMoved > DMA_LEN ? DMA_LEN : length - byteMoved;
		// printf("Sending %ld : %ld/%ld\n", byteToTransfer, byteMoved, length); fflush(NULL);
		byteMoved += read(fd, &buffer[byteMoved], byteToTransfer);
	}
	end = getTime();

	// printf("DONE reading...\n"); fflush(NULL);

	return end - start;
}

unsigned long readDMA_simple(int fd, unsigned char *buffer, unsigned long length){
	unsigned long start, end;
	
	start = getTime();

	read(fd, buffer, length);

	end = getTime();
	return end - start;
}

unsigned long writeDMA(int fd, unsigned char *buffer, unsigned long length){
	unsigned long byteMoved = 0;
	unsigned long byteToTransfer = 0;
	unsigned long start, end;

	// printf("Start writing...\n"); fflush(NULL);

	start = getTime();

	while(byteMoved != length){
		byteToTransfer = length - byteMoved > DMA_LEN ? DMA_LEN : length - byteMoved;
		// printf("Sending %ld : %ld/%ld\n", byteToTransfer, byteMoved, length); fflush(NULL);
		byteMoved += write(fd, &buffer[byteMoved], byteToTransfer);
	}

	// printf("DONE writing\n"); fflush(NULL);

	end = getTime();

	return end-start;
}

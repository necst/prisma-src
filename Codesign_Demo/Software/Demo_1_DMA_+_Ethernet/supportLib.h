/*       ___
 *      /   /\
 *     /___/  \
 *     \___\  /_       \_\_\_         \_  \_\_\_   \_     \_  \_\_\_\_
 *    /   /\\/ /|     \_  \_         \_  \_       \_\_ \_\_  \_    \_
 *   /___/  \_/ |    \_\_\_  \_ \_  \_  \_\_\_   \_  \_ \_  \_\_\_\_
 *   \   \  / \ |   \_      \_\_   \_       \_  \_     \_  \_    \_
 *    \___\//\_\|  \_      \_     \_  \_\_\_   \_     \_  \_    \_
 *     /___/  \
 *     \   \  /
 *      \___\/
 *
 * Copyright 2016 Gianluca Durelli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SUPPORTLIB_H_
#define SUPPORTLIB_H_

#define DMA_LEN 1ULL * 1024

unsigned long writeDMA(int fd, unsigned char *buffer, unsigned long length);
unsigned long readDMA(int fd, unsigned char *buffer, unsigned long length);
unsigned long readDMA_simple(int fd, unsigned char *buffer, unsigned long length);
unsigned long reconfigure(char *bitstream);
unsigned long getTime();

#endif /* SUPPORTLIB_H_ */

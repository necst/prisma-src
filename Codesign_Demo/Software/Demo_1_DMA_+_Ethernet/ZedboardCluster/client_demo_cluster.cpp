/*       ___
 *      /   /\
 *     /___/  \
 *     \___\  /_       \_\_\_         \_  \_\_\_   \_     \_  \_\_\_\_
 *    /   /\\/ /|     \_  \_         \_  \_       \_\_ \_\_  \_    \_
 *   /___/  \_/ |    \_\_\_  \_ \_  \_  \_\_\_   \_  \_ \_  \_\_\_\_
 *   \   \  / \ |   \_      \_\_   \_       \_  \_     \_  \_    \_
 *    \___\//\_\|  \_      \_     \_  \_\_\_   \_     \_  \_    \_
 *     /___/  \
 *     \   \  /
 *      \___\/
 *
 * Copyright 2016 Gea Bianchi, Fabiola Casasopra
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/time.h>
#include <stdio.h>
#include <pthread.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <map>

#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/* Max length of a protein */
#define N 630
/* Max length of a peptide */
#define M 50

/* Commands */
#define LOAD_PROTEIN 0
#define PROTEIN_MATCH 1

#define NUM_CLIENT 2

/* Vector to save the peptides to be matched */
std::vector<std::string> unknownProtein;

/* Number of different peptides to match */
int validPeptides;

typedef struct eth{
    int portno;
    char addr[14];
} eth_data;

/* Read the peptides */
int readPeptides(std::fstream *fPeptides, std::vector<std::string> *unknownProtein){
    std::string line;
    int validPeptides = 0;
    while (std::getline(*fPeptides, line)) {
        size_t pos = line.find_first_of("\t");
        std::string strPeptide = line.substr(pos+1);
        strPeptide = strPeptide + "y";
        if(validPeptides == 0 || strPeptide.compare(unknownProtein->at(validPeptides-1)) != 0){
            unknownProtein->push_back(strPeptide);
            validPeptides++;
        }
    }
    return validPeptides;
}

void *connectionHandler(void *data){
    printf( "Thread %d, %s \n", ((eth_data*)data)->portno, ((eth_data*)data)->addr);
    int sockfd, n, i;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    
    char buffer[256];
    
    /* Create a socket point */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    
    if (sockfd < 0) {
        perror("ERROR opening socket");
        exit(1);
    }
    
    printf("Created a socket point\n");
    
    // server = gethostbyname(argv[1]);
    
    /* if (server == NULL) {
     fprintf(stderr,"ERROR, no such host\n");
     exit(0);
     } */
    
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(((eth_data*)data)->addr);
    serv_addr.sin_port = htons(((eth_data*)data)->portno);
    
    printf("Initialized server\n");
    
    /* Now connect to the server */
    if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("ERROR connecting");
        exit(1);
    }
    
    printf("Connected\n");
    
    /* Now ask for a message from the user, this message
     * will be read by server
     */
    for (i=0; i<unknownProtein.size(); i++) {
        if (i==0 || unknownProtein[i].compare(unknownProtein[i-1])) {
            /* Now ask for a message from the user, this message
             * will be read by server
             */
            
            bzero(buffer,256);
            strcpy(buffer, unknownProtein[i].c_str());
            
            /* Send message to the server */
            n = write(sockfd, buffer, strlen(buffer));
            
            if (n < 0) {
                perror("ERROR writing to socket");
                exit(1);
            }
            
            /* Now read server response */
            bzero(buffer,256);
            n = read(sockfd, buffer, 255);
            
            if (n < 0) {
                perror("ERROR reading from socket");
                exit(1);
            }
        }
    }
    
    bzero(buffer,256);
    strcpy(buffer, "End");
    
    /* Send message to the server */
    n = write(sockfd, buffer, 3);
    
    /* Now read server response */
    bzero(buffer,256);
    n = read(sockfd, buffer, 255);
    
    if (n < 0) {
        perror("ERROR reading from socket");
        exit(1);
    }
    
    std::cout << "message: " << buffer << std::endl;
    
    /* Now read server response */
    bzero(buffer,256);
    n = read(sockfd, buffer, 255);
    
    if (n < 0) {
        perror("ERROR reading from socket");
        exit(1);
    }
    
    std::cout << "message: " << buffer << std::endl;
    
    close(sockfd);
    return 0;
}

int main(int argc, char *argv[]){
    
    int sockfd, n, i;
    pthread_t tid[NUM_CLIENT];
    struct sockaddr_in serv_addr;
    
    char buffer[256];
    
    eth_data data[NUM_CLIENT];
    
    
    /* File to read the peptides of the unknown protein */
    std::fstream fPeptides ("ALBU_HUMAN.txt", std::fstream::in | std::fstream::out);
    
    /* Check if the file is opened correctly */
    if (!fPeptides.is_open()) {
        std::cout << "Error in opening text files" << std::endl;
        exit(0);
    }
    
    /* Number of different peptides to match */
    validPeptides = readPeptides(&fPeptides, &unknownProtein);
    
    if (argc < 1+NUM_CLIENT) {
        fprintf(stderr,"usage %s hostname_1 port_1 ... hostname_NUM_CLIENT port_NUM_CLIENT \n", argv[0]);
        exit(0);
    }
    
    for(i=1; i<=NUM_CLIENT; i++){
        strcpy(data[i-1].addr, argv[(i*2)-1]);
        data[i-1].portno = atoi(argv[i*2]);
        printf("\nportno %d = %d\n", i, data[i-1].portno);
        printf("\naddr %d = %s\n", i, data[i-1].addr);
    }
    
    for(i=0; i<NUM_CLIENT; i++){
        printf("Creating thread!\n");
        if(pthread_create( &tid[i] , NULL ,  connectionHandler , (void*) &data[i]) < 0){
            perror("\ncould not create thread\n");
            return 1;
        }
    }
    
    pthread_exit(NULL);
    
    return 0;
}
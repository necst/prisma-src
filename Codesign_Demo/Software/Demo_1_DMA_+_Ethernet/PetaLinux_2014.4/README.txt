For this PetaLinux version, uImage and devicetree.dtb have been recompiled:
 -> source file from: https://github.com/Xilinx/linux-xlnx
 -> added driver for DMA from: https://github.com/durellinux/ZedBoard_Linux_DMA_driver
    NOTE: the file ds_axidma.c modified:
	line 170:
		old: memcpy(buf, obj_dev->ds_axidma_addr, len);
		new: copy_to_user(buf, obj_dev->ds_axidma_addr, len);
	line 186:
		old: memcpy(obj_dev->ds_axidma_addr, buf, len);
		new: copy_from_user(obj_dev->ds_axidma_addr, buf, len);
/*       ___
 *      /   /\
 *     /___/  \
 *     \___\  /_       \_\_\_         \_  \_\_\_   \_     \_  \_\_\_\_
 *    /   /\\/ /|     \_  \_         \_  \_       \_\_ \_\_  \_    \_
 *   /___/  \_/ |    \_\_\_  \_ \_  \_  \_\_\_   \_  \_ \_  \_\_\_\_
 *   \   \  / \ |   \_      \_\_   \_       \_  \_     \_  \_    \_
 *    \___\//\_\|  \_      \_     \_  \_\_\_   \_     \_  \_    \_
 *     /___/  \
 *     \   \  /
 *      \___\/
 *
 * Copyright 2016 Gea Bianchi, Fabiola Casasopra
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/time.h>
#include <stdio.h>

#include <iostream>
#include <fstream> 
#include <vector>
#include <map>

/* Linux HW support */
#include "supportLib.h"
#include <fcntl.h>

#include <netdb.h>
#include <netinet/in.h>

/* Max length of a protein */
#define N 630
/* Max length of a peptide */
#define M 50

/* Commands */
#define LOAD_PROTEIN 0
#define PROTEIN_MATCH 1

typedef struct {
   std::string UniqueIdentifier;
   std::string EntryName;
   std::string ProteinName;
   std::string Sequence;
} Protein;

/* Read the protein */
void readProtein(std::fstream *fProtein, Protein *protein){
   std::string line;
   while (std::getline(*fProtein, line)) {
        
      /* Read unique identifier */
      size_t startUniqueIdentifier = line.find_first_of("|");
      size_t endUniqueIdentifier = line.find_first_of("|", startUniqueIdentifier+1);
      protein->UniqueIdentifier = line.substr(startUniqueIdentifier+1, endUniqueIdentifier - (startUniqueIdentifier+1));       

      /* Read entry name */
      size_t endEntryName = line.find_first_of(" ", endUniqueIdentifier+1);
      protein->EntryName = line.substr(endUniqueIdentifier+1, endEntryName - (endUniqueIdentifier+1));       	

      /* Read protein name */
      size_t endProteinName = line.find_first_of("=", endEntryName+1);
      protein->ProteinName = line.substr(endEntryName+1, endProteinName - (endEntryName+1) -3);
        	
      /* Read sequence */
      std::streampos pos;
      while (std::getline(*fProtein, line)) {
         if (line.find_first_of(">") == 0) {
            break;
         }
         pos = (*fProtein).tellg();
         protein->Sequence+=line;
      }
	
      protein->Sequence += "z";

      (*fProtein).seekg(pos, std::ios::beg);
   }
}

/* How many int I need to save the char */
unsigned int roundToInt(unsigned int l){
   return l/4 + (l%4!=0);
}

/* Sending core configuration */
void sendCommand(int fd, int cmd){
   writeDMA(fd, (unsigned char *) &cmd, sizeof(int));
}

/* Sending the protein */
void sendProtein(int fd, unsigned char *sequence){
   int i;
   unsigned int proteinLength = 0, proteinLengthRounded = 0;

   proteinLength = strlen((char*) sequence);
   proteinLengthRounded = roundToInt(proteinLength); 

   /* Send protein length */
   writeDMA(fd, (unsigned char *) &proteinLength, sizeof(int));

   /* Padding */
   if((proteinLength%4)!=0)
      for(i=0; i<(4-(proteinLength%4)); i++)
         strcat((char *)sequence, "x");

   /* Send protein sequence */
   writeDMA(fd, (unsigned char *) sequence, proteinLengthRounded * sizeof(int));
}

/* Sending the number of valid peptides */
void sendNumPept(int fd, int num){
   writeDMA(fd, (unsigned char *) &num, sizeof(int));
}

/* Sending peptides */
void sendPeptide(int fd, unsigned char *peptide){
   int i;
   unsigned int peptSize = strlen((char *)peptide);
   unsigned int peptSizeRounded = roundToInt(peptSize);
   unsigned char paddedPeptide[M];

   strcpy((char*)paddedPeptide, (char*)peptide);

   /* Padding */
   if((peptSize%4)!=0)
      for(i=0; i<(4-(peptSize%4)); i++)
         strcat((char *)paddedPeptide, "x");

      /* Send peptide size */
      writeDMA(fd, (unsigned char *) &peptSize, sizeof(int));
	
      /* Send peptide sequence */
      std::cout << "Sending peptide " << peptide << "of size" << peptSize << " to dma " << fd << std::endl;
      writeDMA(fd, (unsigned char *) paddedPeptide, peptSizeRounded * sizeof(int));
}

/* Checking results */
void checkResults(int fd, std::vector<int> *positions){
   int numProtein, position;

   do{
      readDMA(fd, (unsigned char *) &numProtein, sizeof(int));
      //if(numProtein == -1)
         //std::cout << "No match found! Num = " << numProtein << std::endl;

      if(/*r!=2147483647 || */numProtein!= -1){
         readDMA(fd, (unsigned char *) &position, sizeof(int));

	 std::cout << "Save the position of the match " << position << " in the protein ";
	 std::cout << numProtein << std::endl;
	 positions->push_back(position);
         // std::cout << "intermediateResult size is: " << (*intermediateResult).size() << std::endl;
      }
   }while(/*r!=2147483647 || */numProtein!= -1);
}

int main(){

   int sockfd, newsockfd, portno, clilen;
   char buffer[256];
   std::string buff;
   struct sockaddr_in serv_addr, cli_addr;
   int  n;
   int loop = 1;

   /* Vector to save the peptides to be matched */
   std::vector<std::string> unknownProtein; 

   /* Protein to be identified */
   Protein protein;

   /* Vector to save the matching position */
   std::vector<int> positions;

   /* File to read the protein */
   std::fstream fProtein ("ALBU_HUMAN_FASTA.txt", std::fstream::in | std::fstream::out);
   if (!fProtein.is_open()) {
      std::cout << "Error in opening text file" << std::endl;
      exit(0);
   }
   unsigned char prot[N], pept[M]; // Array to send the char
   int i, j, k, p; // Counter
   std::vector<int> position;
   int validPeptides;
   int dma0 = open("/dev/axi-dma1", O_RDWR); // Dmas to send the data and read them
   int dma1 = open("/dev/axi-dma2", O_RDWR);
   int dma2 = open("/dev/axi-dma3", O_RDWR);
   int dma3 = open("/dev/axi-dma4", O_RDWR);
   int dma[4] = {dma0, dma1, dma2, dma3};
   if(dma0<0 || dma1<0 || dma2<0 || dma3<0){
      std::cout << "Error in opening file" << std::endl;
      exit(0);
   }
	
   /* read from file */
    	
   /* protein*/
   readProtein(&fProtein, &protein); 

   /* First call to socket() function */
   sockfd = socket(AF_INET, SOCK_STREAM, 0);
   
   if (sockfd < 0) {
      perror("ERROR opening socket");
      exit(1);
   }
   
   /* Initialize socket structure */
   bzero((char *) &serv_addr, sizeof(serv_addr));
   portno = 5002;
   
   serv_addr.sin_family = AF_INET;
   serv_addr.sin_addr.s_addr = INADDR_ANY;
   serv_addr.sin_port = htons(portno);
   
   /* Now bind the host address using bind() call.*/
   if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
      perror("ERROR on binding");
      exit(1);
   }
      
   /* Now start listening for the clients, here process will
    * go in sleep mode and will wait for the incoming connection
    */
   
   listen(sockfd,5);
   clilen = sizeof(cli_addr);
   
   /* Accept actual connection from the client */
   newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, (socklen_t*)&clilen);
	
   if (newsockfd < 0) {
      perror("ERROR on accept");
      exit(1);
   }

   /* Receiving peptides from the client */

   while(loop){
      /* If connection is established then start communicating */
      bzero(buffer,256);
      n = read( newsockfd,buffer,255 );

      if (n < 0) {
         perror("ERROR reading from socket");
         exit(1);
      }

      buff.assign(buffer,n);

      if (n != 0)
         std::cout << "Here is the message: " << buff << std::endl;

      if (buff.compare(0,3,"End") == 0){
         loop = 0;
	 /* Write a response to the client */
         n = write(newsockfd,"All peptide sent",16);
   	
         if (n < 0) {
            perror("ERROR writing to socket");
            exit(1);
         }
      } else {

         unknownProtein.push_back(buff);
   
         /* Write a response to the client */
         n = write(newsockfd,"Peptide sent",12);
   
         if (n < 0) {
            perror("ERROR writing to socket");
            exit(1);
         }
      }
   }  

   validPeptides = unknownProtein.size();

   std::cout << "validPeptides: " << validPeptides << std::endl;

   int numParts = 4;

   /* communication with hw */

   /* sending protein to the 4 dmas */

   for(i=0; i<numParts; i++){

      // std::cout << "Dma number :" << i << std::endl;
      std::cout << "Sending load command" << std::endl;

      /* Send load command */
      sendCommand(dma[i], LOAD_PROTEIN);

      /* Send the sequence of the protein */
      strcpy((char*)prot, (protein.Sequence).c_str());

      sendProtein(dma[i], prot);

      // std::cout << "Sending match command" << std::endl;

      /* Send match command */
      sendCommand(dma[i], PROTEIN_MATCH);

      /* Send number of peptide I have to match */
      sendNumPept(dma[i], validPeptides);

   }

   j=0;

   /* sending peptides to the 4 dmas and waiting for the results*/
   for(p=0; p<validPeptides; p++){
      strcpy((char *)pept, unknownProtein[p].c_str());
      // for(j=0; j<4; j++){
         std::cout << "send to dma " << j << std::endl;
         sendPeptide(dma[j], pept);
      // }
      j++;

      if(j==4)
         for(i=0; i<4; i++){
            j=0;
            std::cout << "reading results from " << i << std::endl;
            checkResults(dma[i], &positions);
         }
   }
      
   std::cout << "\n Number of peptides identified with KMP algorithm: \n" << std::endl;
   std::cout << positions.size() << "\t" << " out of " << validPeptides;
   std::cout << std::endl;

   buff.assign("\n Number of peptides identified with KMP algorithm: \n");
   buff.append(std::to_string(positions.size()));
   buff.append(" out of ");
   buff.append(std::to_string(validPeptides));
   buff.append(" peptides were found.");

   /* Write a response to the client */
   bzero(buffer,256);
   strcpy(buffer, buff.c_str());

   n = write(newsockfd,buffer,256);
   
   if (n < 0) {
      perror("ERROR writing to socket");
      exit(1);
   }

   return 0;
}


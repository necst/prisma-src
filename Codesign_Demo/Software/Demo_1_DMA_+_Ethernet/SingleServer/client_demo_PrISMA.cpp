/*       ___
 *      /   /\
 *     /___/  \
 *     \___\  /_       \_\_\_         \_  \_\_\_   \_     \_  \_\_\_\_
 *    /   /\\/ /|     \_  \_         \_  \_       \_\_ \_\_  \_    \_
 *   /___/  \_/ |    \_\_\_  \_ \_  \_  \_\_\_   \_  \_ \_  \_\_\_\_
 *   \   \  / \ |   \_      \_\_   \_       \_  \_     \_  \_    \_
 *    \___\//\_\|  \_      \_     \_  \_\_\_   \_     \_  \_    \_
 *     /___/  \
 *     \   \  /
 *      \___\/
 *
 * Copyright 2016 Gea Bianchi, Fabiola Casasopra
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/time.h>
#include <stdio.h>

#include <iostream>
#include <fstream> 
#include <vector>
#include <map>

#include <netdb.h>
#include <netinet/in.h>

/* Max length of a protein */
#define N 630
/* Max length of a peptide */
#define M 50

/* Commands */
#define LOAD_PROTEIN 0
#define PROTEIN_MATCH 1

/* Read the peptides */
int readPeptides(std::fstream *fPeptides, std::vector<std::string> *unknownProtein){
   std::string line;
   int validPeptides = 0;
   while (std::getline(*fPeptides, line)) {
      size_t pos = line.find_first_of("\t");
      std::string strPeptide = line.substr(pos+1);
      strPeptide = strPeptide + "y";
      if(validPeptides == 0 || strPeptide.compare(unknownProtein->at(validPeptides-1)) != 0){
         unknownProtein->push_back(strPeptide);
	 validPeptides++;
      }
   }
   return validPeptides;
}

int main(int argc, char *argv[]){
	
   int sockfd, portno, n;
   struct sockaddr_in serv_addr;
   struct hostent *server;
   
   char buffer[256];

   std::vector<std::string> unknownProtein; // Vector to save the peptides to be matched
   std::vector<int> positions;
   std::fstream fPeptides ("ALBU_HUMAN.txt", std::fstream::in | std::fstream::out); // File to read the peptides of the unknown protein
   if (!fPeptides.is_open()) {
      std::cout << "Error in opening text file" << std::endl;
      exit(0);
   }
   int validPeptides; // Number of different peptides to match
   unsigned char prot[N], pept[M]; // Array to send the char
   int i, j, k, p; // Counter 

   if (argc < 3) {
      fprintf(stderr,"usage %s hostname port \n", argv[0]);
      exit(0);
   }

   portno = atoi(argv[2]);
   
   /* Create a socket point */
   sockfd = socket(AF_INET, SOCK_STREAM, 0);
   
   if (sockfd < 0) {
      perror("ERROR opening socket");
      exit(1);
   }
	
   server = gethostbyname(argv[1]);
   
   if (server == NULL) {
      fprintf(stderr,"ERROR, no such host\n");
      exit(0);
   }
   
   bzero((char *) &serv_addr, sizeof(serv_addr));
   serv_addr.sin_family = AF_INET;
   bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
   serv_addr.sin_port = htons(portno);
   
   /* Now connect to the server */
   if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
      perror("ERROR connecting");
      exit(1);
   }
	
   /* read from file */
      	
   /* peptides */
   validPeptides = readPeptides(&fPeptides, &unknownProtein);

   int numParts = 4;

   /* communication with server */

   for (i=0; i<unknownProtein.size(); i++) {
      if (i==0 || unknownProtein[i].compare(unknownProtein[i-1])) {
         /* Now ask for a message from the user, this message
          * will be read by server
          */

         bzero(buffer,256);
	 strcpy(buffer, unknownProtein[i].c_str());         
        
         /* Send message to the server */
         n = write(sockfd, buffer, strlen(buffer));
   
         if (n < 0) {
            perror("ERROR writing to socket");
            exit(1);
         }
           
         /* Now read server response */
   	 bzero(buffer,256);
   	 n = read(sockfd, buffer, 255);

 	 if (n < 0) {
      	    perror("ERROR reading from socket");
            exit(1);
         }
      }      
   }

   bzero(buffer,256);
   strcpy(buffer, "End");         
        
   /* Send message to the server */
   n = write(sockfd, buffer, 3);

   /* Now read server response */
   bzero(buffer,256);
   n = read(sockfd, buffer, 255);

   if (n < 0) {
      perror("ERROR reading from socket");
      exit(1);
   }

   std::cout << "message: " << buffer << std::endl;

   /* Now read server response */
   bzero(buffer,256);
   n = read(sockfd, buffer, 255);

   if (n < 0) {
      perror("ERROR reading from socket");
      exit(1);
   }

   std::cout << "message: " << buffer << std::endl;

   return 0;
}

